## How to compile

1. cd sandstone-user-service
2. ./gradlew build

## How to run

1. cd sandstone-user-service/build/libs
2. java -jar sandstone-user-service-1.0-SNAPSHOT.jar

## How to run

Swagger url:
http://localhost:2000/api/v1/swagger-ui.html

---

## Unit Test

1. @Test public void addTest()
2. @Test public void updateUserFoundTest()
3. @Test public void updateUserNotFoundTest()
4. @Test public void deleteUserFoundTest()
5. @Test public void deleteUserNotFoundTest()
