package com.sandstone.user.service;

import com.sandstone.user.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    public User add(User user);
    public User get(int id);
    public List<User> getAll(String searchName, Integer isOddEven);
    public User update(User user);
    public void delete(User user);
    public List<User> getAllByName(String name);
}
