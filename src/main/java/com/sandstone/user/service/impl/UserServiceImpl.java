package com.sandstone.user.service.impl;

import com.sandstone.user.model.User;
import com.sandstone.user.repository.UserRepository;
import com.sandstone.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(User user) {
        return userRepository.save(user);
    }

    @Override
    public User get(int id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> getAll(String searchName, Integer isOddEven) {
        if(isOddEven!=null) {
            return userRepository.findAllByNameAndOddEven(searchName, isOddEven.intValue());
        }
        return userRepository.findAllByNameLike(StringUtils.lowerCase(searchName));
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public List<User> getAllByName(String name) {
        return userRepository.findAllByNameLike("%"+name+"%");
    }

}
