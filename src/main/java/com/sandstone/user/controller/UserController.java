package com.sandstone.user.controller;

import com.sandstone.user.delegate.UserDelegate;
import com.sandstone.user.exception.SandstoneDataNotFoundException;
import com.sandstone.user.model.User;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*")
public class UserController {
    private UserDelegate userDelegate;

    public UserController(UserDelegate userDelegate) {
        this.userDelegate = userDelegate;
    }

    @GetMapping(path = "/{id}")
    public User get(@PathVariable String id) {
        return userDelegate.get(Integer.valueOf(id));
    }

    @GetMapping
    public List<User> get(@RequestParam Map<String, String> params) {
        return userDelegate.getAll(params);
    }

    @PostMapping
    public User add(@RequestBody User user) {
        return userDelegate.add(user);
    }

    @PutMapping
    public User update(@RequestBody User user) {
        try {
            return userDelegate.update(user);
        } catch (SandstoneDataNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", ex);
        }
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        try {
            userDelegate.delete(Integer.valueOf(id));
        } catch (SandstoneDataNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", ex);
        }
    }

}
