package com.sandstone.user.repository;

import com.sandstone.user.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    public User findById(int id);

    @Query("select u from User u where lower(u.name) like %:name% and mod(u.id,2)=:isEven order by u.id")
    public List<User> findAllByNameAndOddEven( @Param("name") String name, @Param("isEven") int isEven);

    @Query("select u from User u where lower(u.name) like %:name% order by u.id")
    public List<User> findAllByNameLike(String name);

}
