package com.sandstone.user.delegate;

import com.sandstone.user.exception.SandstoneDataNotFoundException;
import com.sandstone.user.model.User;
import com.sandstone.user.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UserDelegate {

    private UserService userService;

    @Autowired
    public UserDelegate(final UserService userService){
        this.userService = userService;
    }

    public User get(int id) {
        return userService.get(id);
    }

    public List<User> getAll(Map<String,String> params) {
        String searchName= params.containsKey("searchName")?params.get("searchName"):"";
        Integer oddEven = null;
        if(params.containsKey("searchOddEven")) {
            oddEven = (params.get("searchOddEven")).equals("odd")?1:0;
        }
        return userService.getAll(StringUtils.lowerCase(searchName),oddEven);
    }

    public User add(User user) { return userService.add(user); }

    public User update(User user) throws SandstoneDataNotFoundException {
        if(userService.get(user.getId())!=null)
            return userService.update(user);
        throw new SandstoneDataNotFoundException();
    }

    public void delete(int id) throws SandstoneDataNotFoundException {
        User user = userService.get(id);
        if(user==null)
            throw new SandstoneDataNotFoundException();
        userService.delete(user);

    }
}
