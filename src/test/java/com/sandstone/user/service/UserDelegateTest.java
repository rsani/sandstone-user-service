package com.sandstone.user.service;

import com.sandstone.user.config.UserDelegateTestConfig;
import com.sandstone.user.delegate.UserDelegate;
import com.sandstone.user.exception.SandstoneDataNotFoundException;
import com.sandstone.user.model.User;
import com.sandstone.user.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {UserDelegateTestConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDelegateTest {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    UserDelegate userDelegate;

    @Test
    public void addTest() {
        String name = "Rizky Sani";
        User user = new User();
        user.setName(name);
        when(userService.add(user)).thenReturn(user);
        User userDB = userService.add(user);
        assertEquals(user.getName(), userDB.getName());
    }


    @Test
    public void updateUserFoundTest() throws SandstoneDataNotFoundException {
        User userDB = new User();
        userDB.setId(1);
        userDB.setName("Rizky");

        User userRequest = new User();
        userRequest.setId(1);
        userRequest.setName("Rizky Sani");

        when(userService.get(userDB.getId())).thenReturn(userDB);
        when(userService.update(userRequest)).thenReturn(userRequest);
        User userUpdateDB = userDelegate.update(userRequest);
        assertEquals(userRequest.getName(), userUpdateDB.getName());
    }

    @Test
    public void updateUserNotFoundTest() {
        User userDB = new User();
        userDB.setId(1);
        userDB.setName("Rizky");

        User userRequest = new User();
        userRequest.setId(1);
        userRequest.setName("Rizky Sani");

        when(userService.get(userDB.getId())).thenReturn(null);
        when(userService.update(userRequest)).thenReturn(userRequest);

        User userUpdateDB = null;
        try {
            userUpdateDB = userDelegate.update(userRequest);
        } catch (SandstoneDataNotFoundException e) {
            e.printStackTrace();
        }
        assertEquals(null, userUpdateDB);
    }

    @Test
    public void deleteUserFoundTest() {
        User user = new User();
        user.setId(1);
        user.setName("Rizky");
        boolean isUserFound = true;

        when(userService.get(user.getId())).thenReturn(user);
        try {
            userDelegate.delete(user.getId());
        } catch (SandstoneDataNotFoundException e) {
            isUserFound = false;
        }
        assertTrue(isUserFound);
    }

    @Test
    public void deleteUserNotFoundTest() {
        User user = new User();
        user.setId(1);
        user.setName("Rizky");
        boolean isUserFound = true;

        when(userService.get(user.getId())).thenReturn(null);
        try {
            userDelegate.delete(user.getId());
        } catch (SandstoneDataNotFoundException e) {
            isUserFound = false;
        }
        assertFalse(isUserFound);
    }
}
