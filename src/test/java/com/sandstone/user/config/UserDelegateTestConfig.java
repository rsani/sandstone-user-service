package com.sandstone.user.config;

import com.sandstone.user.delegate.UserDelegate;
import com.sandstone.user.repository.UserRepository;
import com.sandstone.user.service.UserService;
import com.sandstone.user.service.impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class UserDelegateTestConfig {

    @Bean
    public UserRepository userRepository(){return mock(UserRepository.class);}

    @Bean
    public UserService userService(){return new UserServiceImpl(userRepository());}

    @Bean
    public UserDelegate user(){return new UserDelegate(userService());}
}
